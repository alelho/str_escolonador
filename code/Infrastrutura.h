#define MAX_TAREF 26
#define MAX_SIMUL 2048
#define MAX_T_SLACK 2048
#define MAIUSCULA 65
#define MINUSCULA 97
#define IDLE -19

typedef struct
{
	unsigned id;
	unsigned c;
	unsigned p;
	unsigned d;
	int t_slack;
	unsigned c_rest;
	unsigned t_periodo;
	unsigned t_deadline;
	bool atrasado;
} TAREFA;


