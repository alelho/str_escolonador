#include <cstring>
#include "Lst.h"

int		_preempcoes, _trocaContexto;
char	_diagrama[MAX_SIMUL + 1];

void Executa_Escalonamento(int totalTarefas, int tempoExec, TAREFA tarefas[MAX_TAREF])
{
	unsigned time = _preempcoes = _trocaContexto = 0;
	memset(_diagrama, '0', sizeof _diagrama); // preenche com '0's o diagrama.
	int id_ultimaTarefaExecutada = -1;
	int id_tarefa;

	while( time <= tempoExec )
	{
		/* acrescenta o per�odo novamente */
		for (int i = 0; i < totalTarefas; i++)
		{
			if( tarefas[i].t_periodo == 0 )
			{
				tarefas[i].t_periodo = tarefas[i].p;		
				tarefas[i].c_rest = tarefas[i].c;
				tarefas[i].t_deadline = time + tarefas[i].d;
			}

			/* calcula o t_slack das tarefas */
			tarefas[i].t_slack = tarefas[i].t_deadline - time - tarefas[i].c_rest;
		}
		/***********************************************************************************************/

		/* obtem o id da tarefa a ser executada */
		int t_slack = MAX_T_SLACK;
		id_tarefa = IDLE;
		for (int i = 0; i < totalTarefas; i++)
		{
			if (tarefas[i].c_rest > 0 && tarefas[i].t_slack < t_slack)
			{
				id_tarefa = tarefas[i].id;
				t_slack = tarefas[i].t_slack;
			}
		}
		/***********************************************************************************************/

		/* verifica se a tarefa esta em atraso */
		if (tarefas[id_tarefa].t_slack < 0 && tarefas[id_tarefa].c_rest > 0 && time >= tarefas[id_tarefa].t_deadline)
			tarefas[id_tarefa].atrasado = true;
		else
			tarefas[id_tarefa].atrasado = false;
		/***********************************************************************************************/

		/* Executa a computa��o */
		if (id_tarefa >= 0)
		{
			tarefas[id_tarefa].c_rest--;

			// calculo de preemp��o
			if (id_tarefa != id_ultimaTarefaExecutada)
			{
				if (id_ultimaTarefaExecutada > -1)
				{
					if (tarefas[id_ultimaTarefaExecutada].c_rest > 0 && tarefas[id_ultimaTarefaExecutada].c_rest < tarefas[id_ultimaTarefaExecutada].c && time > 0 && time <= tempoExec)
						_preempcoes++;
				}
			}

			// calculo de troca de contexto
			if ( id_tarefa != id_ultimaTarefaExecutada && time > 0)
				_trocaContexto++;
			else if (id_tarefa == id_ultimaTarefaExecutada) 
			{
				if (tarefas[id_tarefa].t_periodo == tarefas[id_tarefa].p) // Quando � a mesma tarefa, ent�o verifica se come�ou uma nova execu��o em outro contexto. Ex.: BB|B
					_trocaContexto++;
			}
		}
		else // Idle
		{
			if (id_tarefa != id_ultimaTarefaExecutada && time > 0)
			{
				if (time < tempoExec)
					_preempcoes++;

				_trocaContexto++;
			}
		}
		id_ultimaTarefaExecutada = id_tarefa;
		/***********************************************************************************************/

		/* decrementa uma unidada de tempo do periodo auxiliar de cada tarefa */
		for (int i = 0; i < totalTarefas; i++)
		{
			tarefas[i].t_periodo--;
		}
		/***********************************************************************************************/

		/* Diagarama de Gantt */
		if( time <= tempoExec - 1 )
		{
			if( tarefas[id_tarefa].atrasado )
				_diagrama[time] = MINUSCULA + id_tarefa;
			else
				_diagrama[time] = MAIUSCULA + id_tarefa;
		}
		/***********************************************************************************************/

		time++;
	}

	_diagrama[time-1] = '\0';
}

int Get_TotalPreempcoes()
{
	return _preempcoes;
}

int Get_TotalTrocaContexto()
{
	return _trocaContexto;
}

char *Get_GraficoGantt()
{
	return _diagrama;
}
