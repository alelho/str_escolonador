/*****************************************************************************************
* Trabalho I da disciplina de sistema de tempo real										 *
* Professo: Roland																		 *
* Aluno: Alexandre Carvalho																 *
* Data: 2016/1																			 *
* �ltima revis�o: 01/2017																 *
******************************************************************************************
* O programa simula o escalonamento Least Slack Time First (LST) de processos. V�rios  	 *
* processos disputam o processador para poder executar suas instru��es, o sistema opera- *
* cional implementa um tipo de escalonamento para executar estes processos.				 *
*****************************************************************************************/

#include <iostream>
#include <cstring>
#include "Lst.h"

using namespace std;

int main()
{
	unsigned	numTarefas, tempoSimulacao;
	TAREFA		tarefa[MAX_TAREF];
	char		diagrama[MAX_SIMUL + 1];
	unsigned	d_index;
	unsigned	numPreemp, numChav;
	unsigned	i, j;

	while (1)
	{
		/* LEITURA */
		cin >> numTarefas >> tempoSimulacao;
		if (numTarefas == 0 || tempoSimulacao == 0)
			break;

		for (i = 0; i < numTarefas; ++i)
		{
			tarefa[i].id = i;
			tarefa[i].t_slack = 0;
			cin >> tarefa[i].c >> tarefa[i].p >> tarefa[i].d;
			tarefa[i].c_rest = tarefa[i].c;
			tarefa[i].t_periodo = tarefa[i].p;
			tarefa[i].t_deadline = tarefa[i].d;
			tarefa[i].atrasado = false;
		}

		Executa_Escalonamento(numTarefas, tempoSimulacao, tarefa);

		/* SIMULACAO */
		numPreemp = Get_TotalPreempcoes();
		numChav = Get_TotalTrocaContexto();

		strncpy(diagrama, Get_GraficoGantt(), sizeof diagrama - 1);

		/* IMPRESSAO DE RESULTADOS */
		cout << diagrama << endl;
		cout << numPreemp << " " << numChav << endl;

		cout << endl;
	}
	return 0;
}
