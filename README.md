Escalonador de processos - Least Slack Time First (LST) - Versão Linux

O trabalho foi proposto na disciplina de sistema de tempo real e codificado na linguagem C++.

Em um sistema operacional existem vários processos sendo executados e cada um precisa obter o processador para
realizar sua execução. O sistema operacional é responsável por definir qual processo irá obter o processar para
executar, geralmente é implementado no sistema operacional algum escalonamento que é responsável por escolher o
processo a ser executado. Existe diversos modelos de escalonamento, neste trabalho iremos utilizar o escalonador
Least Slack Time First (LST).
Mais informações:
https://pt.wikipedia.org/wiki/Sistema_operacional_de_tempo-real
http://www.dca.ufrn.br/~affonso/DCA_STR/aulas/escalonamento-STR-parte1.pdf


Least Slack Time First (LST): O precesso com menor tempo de folga (Tslack) recebe maior prioridade para executar.
Processo A(TEMPO_EXECUCAO, PERIODO, DEADLINE)
Tslack = D - t - Crest
D = deadline do processo
t = tempo
Crest = tempo de execução que resta para o processo em um periodo

Execuçao do programa:
- Abrir o terminal do Linux
- Ir na pasta 'STR-Escalonador\code\'
- Executar o comando 'make' para rodar makefile para compilação do programa, irá gerar um arquivo chamado 'Simulador'
- Através do comando './Simulador' é possível executar o programa

Input:
O programa espera dois valoers inteiros para digitar: Total_Processos e Total_tempo.
2 20 -> 2 processos, e irá executar até a unidade de tempo 20.
Agora tem que digitar os processos, neste caso 2 porque digitamos o valor 2 para o Total_Processos.
2 4 4 -> Tempo de execução 2, período 4 e deadline 4
5 10 10 -> Tempo de execução 5, período 10 e deadline 10

Output para o input acima:
AABBAABBBAABAABBABAB
5 12 -> 5 preempções e 12 trocas de contexto

Preempção:
Um processo A está executando, mas um processo B no tempo 10 ficou com prioridade maior, então o processo A perde
o processador e o processo B ganha o direito de executar, dizemos então que o processo A foi preempitado.

Troca de contexto:
Quando ocorre a troca de processo a ser executado no processador, exemplo: AABCCA, existem 3 trocas de contexto
Processo A para processo B, processo B para processo C e por último processo C para processo A.